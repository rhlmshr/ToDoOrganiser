package com.example.rahulmishra.todo_organizer;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.Switch;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import android.widget.TextView;
import android.widget.TimePicker;

public class Add_layout extends AppCompatActivity {

    TextView titleAdd, descAdd, dateAdd, timeAdd ;
    Switch timeTrue ;
    Button okBtn,clearBtn ;
    Calendar time ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_layout);

        time = Calendar.getInstance() ;
        time.set(Calendar.SECOND, 0) ;
        time.set(Calendar.MILLISECOND, 0) ;

        titleAdd = (TextView) findViewById(R.id.titleAddDialog);
        descAdd = (TextView) findViewById(R.id.descAddDialog);
        okBtn = (Button) findViewById(R.id.button);
        dateAdd = (TextView) findViewById(R.id.dateAddDialog);
        timeAdd = (TextView) findViewById(R.id.timeAddDialog) ;
        clearBtn = (Button) findViewById(R.id.clearBtn);

        dateAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar calendar = Calendar.getInstance();
                DatePickerDialog datePickerDialog = new DatePickerDialog(Add_layout.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        time.set(year,month,dayOfMonth);
                        dateAdd.setText(new SimpleDateFormat("dd-MM-yyyy").format(time.getTime()));
                    }
                },calendar.get(Calendar.YEAR),calendar.get(Calendar.MONTH),calendar.get(Calendar.DAY_OF_MONTH));
                datePickerDialog.show();
            }
        });


        timeAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar calendar = Calendar.getInstance();
                TimePickerDialog timePickerDialog = new TimePickerDialog(Add_layout.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int hour_of_day, int minute) {
                        time.set(Calendar.HOUR_OF_DAY, hour_of_day);
                        time.set(Calendar.MINUTE, minute);
                        timeAdd.setText(new SimpleDateFormat("hh:mm a").format(time.getTime()));
                    }
                }, calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), false);
                timePickerDialog.show();
            }
        });

        timeTrue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(timeTrue.isChecked()) {
                    timeAdd.setOnClickListener(null);
                    dateAdd.setOnClickListener(null);
                    timeAdd.setText("");
                    dateAdd.setText("");
                } else  {
                    dateAdd.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Calendar calendar = Calendar.getInstance();
                            DatePickerDialog datePickerDialog = new DatePickerDialog(Add_layout.this, new DatePickerDialog.OnDateSetListener() {
                                @Override
                                public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                                    time.set(year,month,dayOfMonth);
                                    dateAdd.setText(new SimpleDateFormat("dd-MM-yyyy").format(time.getTime()));
                                }
                            },calendar.get(Calendar.YEAR),calendar.get(Calendar.MONTH),calendar.get(Calendar.DAY_OF_MONTH));
                            datePickerDialog.show();
                        }
                    });


                    timeAdd.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Calendar calendar = Calendar.getInstance();
                            TimePickerDialog timePickerDialog = new TimePickerDialog(Add_layout.this, new TimePickerDialog.OnTimeSetListener() {
                                @Override
                                public void onTimeSet(TimePicker timePicker, int hour_of_day, int minute) {
                                    time.set(Calendar.HOUR_OF_DAY, hour_of_day);
                                    time.set(Calendar.MINUTE, minute);
                                    timeAdd.setText(new SimpleDateFormat("hh:mm a").format(time.getTime()));
                                }
                            }, calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), false);
                            timePickerDialog.show();
                        }
                    });
                }
            }
        });

        clearBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                timeAdd.setText("") ;
                dateAdd.setText("") ;
                titleAdd.setText("") ;
                descAdd.setText("") ;
                timeTrue.setChecked(false) ;
                time = Calendar.getInstance();
            }
        });

        okBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(titleAdd == null)    {
                    Snackbar.make(v, "Add Title", Snackbar.LENGTH_SHORT).show() ;
                    return ;
                } else  {

                }

            }
        });
    }
}
