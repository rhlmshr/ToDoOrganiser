package com.example.rahulmishra.todo_organizer;

/**
 * Created by Rahul Mishra on 14-07-2017.
 */

public class IntentConstants {
    public static final String NEW_TODO = "new_todo" ;
    public static final int NEW_TODO_REQ = 1 ;
}
