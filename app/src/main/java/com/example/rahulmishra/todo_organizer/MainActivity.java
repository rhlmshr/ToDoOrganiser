package com.example.rahulmishra.todo_organizer;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewStub;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    ArrayList<ToDoEntry> toDoEntries ;
    private TodoViewAdapter todoViewAdapter;
    RecyclerView recyclerView ;
    ViewStub startingLayout ;
    

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        todoViewAdapter = new TodoViewAdapter(this, toDoEntries) ;
        getToDoEntries() ;

        if(toDoEntries.size() == 0) {
            startingLayout = (ViewStub) findViewById(R.id.empty_list);
            startingLayout.inflate() ;

            final ImageView imagebtn = (ImageView) findViewById(R.id.imageButton);
            
            imagebtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(MainActivity.this, Add_layout.class) ;
                    startActivityForResult(intent, );
                }
            });
            
        } else  {
            startingLayout = (ViewStub) findViewById(R.id.not_empty_list);
            startingLayout.inflate() ;
            
            recyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);
            recyclerView.setAdapter(todoViewAdapter) ;
            recyclerView.setHasFixedSize(true) ;
            recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)) ;
        }
        
    }

    private List<ToDoEntry> getToDoEntries() {
        toDoEntries = new ArrayList<>();
        ToDoOpenHelper toDoOpenHelper = ToDoOpenHelper.getTodoOpenHelper(this) ;
        SQLiteDatabase database = toDoOpenHelper.getReadableDatabase() ;
        Cursor cursor = database.query(ToDoOpenHelper.TODO_TABLE_NAME, null, null, null, null, null, null) ;
        while(cursor.moveToNext())  {
            String title = cursor.getString(cursor.getColumnIndex(ToDoOpenHelper.TODO_TITLE)) ;
            //String category = cursor.getString(cursor.getColumnIndex(ToDoOpenHelper.TODO_CATEGORY)) ;
            String desc = cursor.getString(cursor.getColumnIndex(ToDoOpenHelper.TODO_DESC)) ;
            long time = cursor.getLong(cursor.getColumnIndex(ToDoOpenHelper.TODO_TIME)) ;
            ToDoEntry toDoEntry = new ToDoEntry(title, desc, time) ;
            toDoEntries.add(toDoEntry) ;
        }
        todoViewAdapter.notifyDataSetChanged();
        return toDoEntries;
    }
}