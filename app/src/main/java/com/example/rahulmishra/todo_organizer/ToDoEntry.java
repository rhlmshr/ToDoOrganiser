package com.example.rahulmishra.todo_organizer;

import android.util.Log;

import java.sql.Time;
import java.util.Date;

/**
 * Created by Rahul Mishra on 01-07-2017.
 */

public class ToDoEntry {
    private String title ;
    private String desc ;
    private int id ;
    private long time ;

    public ToDoEntry(String title, String desc, long time) {
        this.title = title;
        this.desc = desc;
        this.time = time;
        Log.i("Log", "ToDoEntry: " + "Object created");
    }

    public ToDoEntry(String title, String desc) {
        this.title = title;
        this.desc = desc;
    }

    public ToDoEntry(String title) {
        this.title = title;
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getTimeinString() {
        return new java.text.SimpleDateFormat("hh:mm a").format(time) ;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public String getDateinString()   {
        return new java.text.SimpleDateFormat("dd-MM-yyyy").format(time) ;
    }
}
