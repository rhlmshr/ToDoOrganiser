package com.example.rahulmishra.todo_organizer;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by Rahul Mishra on 07-07-2017.
 */

public class ToDoOpenHelper extends SQLiteOpenHelper {
    public final static String TODO_TABLE_NAME = "main" ;
    public final static String TODO_ID = "_id" ;
    public final static String TODO_TITLE = "title" ;
    public final static String TODO_DESC = "desc" ;
    public final static String TODO_CATEGORY = "category" ;
    public final static String TODO_TIME = "time" ;
    public static ToDoOpenHelper toDoOpenHelper ;

    public static ToDoOpenHelper getTodoOpenHelper(Context context) {
        if(toDoOpenHelper == null)  {
            toDoOpenHelper = new ToDoOpenHelper(context) ;
        }
        return toDoOpenHelper ;
    }

    private ToDoOpenHelper(Context context) {
        super(context, "todoDB", null, 1) ;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String query = "create table " + TODO_TABLE_NAME + "(" + TODO_ID
                + " integer primary key autoincrement, " + TODO_TITLE + " text, "
                + TODO_CATEGORY + " text, " + TODO_DESC + " text, " + TODO_TIME +
                " integer" + ");" ;
        db.execSQL(query) ;

        String query1 = "ALTER TABLE " + TODO_TABLE_NAME + " autoincrement=1001;" ;
        db.execSQL(query1) ;
        Log.i("Log", "onCreate: Database Created");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
