package com.example.rahulmishra.todo_organizer;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Rahul Mishra on 13-07-2017.
 */

class TodoViewAdapter extends RecyclerView.Adapter<TodoViewAdapter.ViewHolder> {
    Context context ;
    ArrayList<ToDoEntry> toDoEntries ;

    public TodoViewAdapter(Context context, ArrayList<ToDoEntry> toDoEntries) {
        this.context = context;
        this.toDoEntries = toDoEntries ;
        Log.i("Log", "TodoViewAdapter: Adapted Constructed");
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        protected TextView iconText, tvTitle, time, date ;

        public ViewHolder(View itemView) {
            super(itemView);
            tvTitle = (TextView) itemView.findViewById(R.id.tvTitle);
            iconText = (TextView) itemView.findViewById(R.id.iconText);
            time = (TextView) itemView.findViewById(R.id.date);
            date = (TextView) itemView.findViewById(R.id.time);
        }
    }

    @Override
    public TodoViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.todo_item, null) ;
        ViewHolder mHolder = new ViewHolder(view) ;
        return mHolder ;
    }

    @Override
    public void onBindViewHolder(TodoViewAdapter.ViewHolder holder, int position) {
        holder.iconText.setText(toDoEntries.get(position).getTitle().charAt(0));
        holder.tvTitle.setText(toDoEntries.get(position).getTitle()) ;
        holder.date.setText(toDoEntries.get(position).getDateinString());
        holder.time.setText(toDoEntries.get(position).getTimeinString());
    }

    @Override
    public int getItemCount() {
        return toDoEntries.size() ;
    }
}
